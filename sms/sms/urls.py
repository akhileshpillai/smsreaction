from django.conf.urls import patterns, include, url
from django.contrib import admin
#from onreceivesms.views import sms

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sms.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls), name='admin-url'),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login'),
    url(r'^', include('onsmsreceive.urls')),
)
