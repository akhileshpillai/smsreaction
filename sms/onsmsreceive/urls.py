from django.conf.urls import patterns, url

urlpatterns = patterns('onsmsreceive.views',
    url(r'^sms/$', 'sms'),
    url(r'^list/$', 'listing',name='home'),
    url(r'^dailycheckin/$','dailyCheckin',name='daily-checkin'),
    url(r'^dailytotal/$','dailytotal', name='daily-total'),
)

