# Create your models here.
from django.db import models
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.core.cache import cache
from time import strftime
from django.db.models.signals import post_save
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime
from django_twilio.models import Caller

class SmsMessage(models.Model):
     callerid = models.ForeignKey(Caller)
     timestamp = models.DateTimeField(auto_now_add=True)
     datestamp = models.DateField(blank=True)
     sender = models.CharField(max_length=255)
     message = models.CharField(max_length=255,blank=True)

     def __str__(self):
        return '%s %s %s' % ( self.sender, self.timestamp, self.message)


class SmsDailyTotal(models.Model):
     callerid = models.ForeignKey(Caller)
     timestamp = models.DateTimeField()
     datestamp = models.DateField(blank=True)
     delta_ts = models.FloatField(blank=True)
     count = models.IntegerField(blank=True)

     def __str__(self):
        return '%s %s %s' % (self.callerid, self.datestamp, self.timestamp)

def calculateDelta(new,old):
        delta = float(new.hour - old.hour) + float(new.minute - old.minute)/float(60)
        print "delta : %s " % ( delta)
        return round(delta,2)

    
def update_date(sender, instance, created, **kwargs):
        ps_timestamp = instance.timestamp
        print "ps_timestamp: %s " % ( ps_timestamp )
        
        ps_datestamp = datetime.date(ps_timestamp)
        print "ps_datestamp: %s " % ( ps_datestamp )
        
	try:
       	    sender = Caller.objects.get(phone_number=instance.sender)
    	except Caller.DoesNotExist:
            print "Caller %s does not exist" % (smsfrom)
        
        
        try:
            updatedRow = SmsMessage.objects.filter(timestamp=ps_timestamp).update(datestamp=ps_datestamp)
            obj, newlycreated = SmsDailyTotal.objects.get_or_create(datestamp=ps_datestamp,callerid=sender,defaults={'delta_ts':0,'count':1,'timestamp':ps_timestamp })
            print " newly created:%s " % (newlycreated) 
            if not newlycreated:
                print " not newly created:%s " % (newlycreated) 
                print " not newly created ps_timestamp: %s " % (ps_timestamp) 
                print " not newly created obj.timestamp:%s " % (obj.timestamp) 
                obj.count = obj.count + 1
                if (obj.count % 2) == 0:
                    obj.delta_ts = obj.delta_ts + calculateDelta(ps_timestamp, obj.timestamp)

                obj.timestamp = ps_timestamp
                obj.save()           
        except ObjectDoesNotExist:
                print("Some problem with smsMessageupdate or smsDailyTotal updates .")

post_save.connect(update_date, sender=SmsMessage, dispatch_uid="update_date")
