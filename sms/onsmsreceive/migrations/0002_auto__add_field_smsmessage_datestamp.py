# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SmsMessage.datestamp'
        db.add_column(u'onsmsreceive_smsmessage', 'datestamp',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 4, 30, 0, 0), blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SmsMessage.datestamp'
        db.delete_column(u'onsmsreceive_smsmessage', 'datestamp')


    models = {
        u'onsmsreceive.smsmessage': {
            'Meta': {'object_name': 'SmsMessage'},
            'datestamp': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'fullname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['onsmsreceive']