# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SmsDailyTotal'
        db.create_table(u'onsmsreceive_smsdailytotal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('callerid', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_twilio.Caller'])),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')()),
            ('datestamp', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('delta_ts', self.gf('django.db.models.fields.FloatField')(blank=True)),
            ('count', self.gf('django.db.models.fields.IntegerField')(blank=True)),
        ))
        db.send_create_signal(u'onsmsreceive', ['SmsDailyTotal'])

        # Deleting field 'SmsMessage.datestamp'
        db.delete_column(u'onsmsreceive_smsmessage', 'datestamp')

        # Deleting field 'SmsMessage.fullname'
        db.delete_column(u'onsmsreceive_smsmessage', 'fullname')

        # Adding field 'SmsMessage.callerid'
        db.add_column(u'onsmsreceive_smsmessage', 'callerid',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=99, to=orm['django_twilio.Caller']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'SmsDailyTotal'
        db.delete_table(u'onsmsreceive_smsdailytotal')

        # Adding field 'SmsMessage.datestamp'
        db.add_column(u'onsmsreceive_smsmessage', 'datestamp',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 1, 1, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'SmsMessage.fullname'
        db.add_column(u'onsmsreceive_smsmessage', 'fullname',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Deleting field 'SmsMessage.callerid'
        db.delete_column(u'onsmsreceive_smsmessage', 'callerid_id')


    models = {
        u'django_twilio.caller': {
            'Meta': {'object_name': 'Caller'},
            'blacklisted': ('django.db.models.fields.BooleanField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'phone_number': ('phonenumber_field.modelfields.PhoneNumberField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'onsmsreceive.smsdailytotal': {
            'Meta': {'object_name': 'SmsDailyTotal'},
            'callerid': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['django_twilio.Caller']"}),
            'count': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'datestamp': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'delta_ts': ('django.db.models.fields.FloatField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'onsmsreceive.smsmessage': {
            'Meta': {'object_name': 'SmsMessage'},
            'callerid': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['django_twilio.Caller']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['onsmsreceive']