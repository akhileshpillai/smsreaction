from django.db import models
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.core.cache import cache
from django.contrib.auth.models import User
from time import strftime
from djangotoolbox.fields import ListField
from djangotoolbox.fields import EmbeddedModelField
from postvideo.forms_ext import StringListField
from django.db.models.signals import post_save
from django.core.exceptions import ObjectDoesNotExist
from bson import ObjectId
from easy_thumbnails.fields import ThumbnailerImageField
# Create your models here.


class CategoryField(ListField):
    def formfield(self, **kwargs):
        return models.Field.formfield(self, StringListField, **kwargs)


def content_file_name(instance, filename):
    part_name = "videos/" 
    filename = strftime("%y%m%d%H%M%S") + filename
    owner =  str(instance.owner) 
    return '/'.join([part_name, owner, filename])

class PostVideo(models.Model):
    video_name = models.FileField(upload_to=content_file_name,) 
    image_thumbnail = ThumbnailerImageField(upload_to=content_file_name,)
    tag = CategoryField() 
    owner = models.CharField(max_length=255,) 

    def __str__(self):
        return ' '.join([self.video_name, self.tag,])

    def get_absolute_url(self):
        return reverse('contacts-view', kwargs={'pk': self.id})

class Tags(models.Model):
     tag = models.CharField(max_length=255,unique=True)
     video_ids = CategoryField()

def add_tag(sender, instance, created, **kwargs):
        
        videoname=instance.video_name
        try:
               objPost = PostVideo.objects.get(video_name=videoname)
        except ObjectDoesNotExist:
                print("Either the entry or blog doesn't exist.")
        videoid = ObjectId(objPost.id) 
        print "videoid : " ,videoid
        print "thumbnail Image URl : " , instance.image_thumbnail['avatar'].url
        for tag in instance.tag:
            obj, created = Tags.objects.get_or_create(tag=tag, defaults={'video_ids': [videoid]})
            if not created:
                obj.video_ids.append(videoid)
                obj.save()
           
post_save.connect(add_tag, sender=PostVideo, dispatch_uid="update_tag_collection")

