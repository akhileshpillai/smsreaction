from django.shortcuts import render
from django.views.generic import ListView
from django.core.urlresolvers import reverse
from django.views.generic import CreateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from postvideo.models import PostVideo
from postvideo.forms import PostVideoForm
from postvideo.models import Tags
from postvideo.serializers import PostVideoSerializer
from postvideo.serializers import TagsSerializer
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
# Create your views here.
from bson import ObjectId
from bson import json_util
import json
from rest_framework.decorators import api_view 
from rest_framework.decorators import authentication_classes
from rest_framework.decorators import permission_classes 

class LoggedInMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoggedInMixin, self).dispatch(*args, **kwargs)

class PostVideoView(LoggedInMixin,ListView):
    model = PostVideo
    template_name = 'video_list.html'
    def get_queryset(self):
        return PostVideo.objects.filter(owner=self.request.user)


class CreateVideoView(CreateView):
    model = PostVideo
    template_name = 'edit_video.html'
    def get_success_url(self):
        return reverse('video-list')

class PostVideoFormView(LoggedInMixin, CreateView):
    model = PostVideo
    template_name = 'edit_video.html'
    form_class = PostVideoForm
    def form_valid(self,form):
        taglist = []
        data = form.save(commit=False)
        taglist = data.tag
        videourl = data.video_name.url
        print "videourl : " + videourl 
        data.owner = self.request.user
        data.save()
        return HttpResponseRedirect(reverse('video-list'))
    def get_success_url(self):
        return reverse('video-list')

class JSONResponse(HttpResponse):
	def __init__(self, data,mongoobjectid=False,**kwargs):
            content = data
            if not mongoobjectid:
                content = JSONRenderer().render(data)
            kwargs['content_type'] = 'application/json'
            super(JSONResponse, self).__init__(content, **kwargs)

@csrf_exempt
@api_view(['GET'])
def tags_video_list(request):
    """
    List all code posted_video, or create a new posted_video.
    """
    if request.method == 'GET':
        tag_videolist = Tags.objects.all()
        serializer = TagsSerializer(tag_videolist, many=True)
        data = json_util.dumps(serializer.data,default=json_util.default)
        return JSONResponse(data,True)

@api_view(['GET'])
@csrf_exempt
def tags_detail(request, tag):
    """
    Retrieve a list of posted_video associated with a tag.
    """
    try:
        print "pk : ", tag
        video_asso_tag = Tags.objects.get(tag=tag)
    except Tag.DoesNotExist:
        return HttpResponse(status=404)
    if request.method == 'GET':
        serializer = TagsSerializer(video_asso_tag)
        data = json_util.dumps(serializer.data,default=json_util.default)
        print data
        return JSONResponse(data,True)

@csrf_exempt
@api_view(['GET','PUT','POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def posted_video_list(request):
    """
    List all code posted_video, or create a new posted_video.
    """
    if request.method == 'GET':
        posted_video = PostVideo.objects.all()
        serializer = PostVideoSerializer(posted_video, many=True)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT' or request.method == 'POST':
        print "request.FILES", request.FILES
        print "request.POST", request.POST
        form = PostVideoForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            data.owner = request.user
            data.save()
            return HttpResponse(status=200)
        return HttpResponse(status=400)
        

@api_view(['GET'])
@csrf_exempt
def posted_video_detail(request, pk):
    """
    Retrieve, update or delete a code posted_video.
    """
    try:
        print "pk : ", pk
        posted_video = PostVideo.objects.get(id=pk)
    except PostVideo.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = PostVideoSerializer(posted_video)
        return JSONResponse(serializer.data)

    elif request.method == 'DELETE':
        posted_video.delete()
        return HttpResponse(status=204)
